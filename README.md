# Lelpo: Lua Equals Lua Plus One

Lelpo is a programming language designed to have all the features of Lua and then some. As Lua, (as well as Lelpo), does not have the ++ increment operator, a name like "Lua++" or "L++" seemed inaccurate. Therefore Lelpo gets its name from the equivalent expression in Lua: `lua = lua + 1`.

For now, this repository is a prototype compiler written in Lua. The final compiler and VM will be written in C, but for now I find it easier to work quickly with Lua.

## Features
(I'll write this eventually)

## Dependencies
* [Lua 5.3.3](https://www.lua.org/download.html)

## Contributing
There are several ways you can contribute to Lelpo:
+ Request access to this repository
+ Fork this repository and send a pull request with your contributions

Feel free to contact me on Twitter:
[@tgrehawi](twitter.com)
