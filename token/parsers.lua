require "token"

local tp = token.parsers

local digits = {}

for i = 0, 9 do
	digits[tostring(i)] = i
end

tp.setcolor("#B")

function tp:identifier(c, fragment)
	local newfrag = fragment..c
	if not newfrag:match('^'.."[%a_][%w_]*"..'$') then
		if #fragment == 0 then
			return nil, true
		else
			return fragment
		end
	end
end

tp["..."] = "varargs"

tp.setcolor("#G")

tp["local"] = "keyword"
tp["function"] = "keyword"
tp["return"] = "keyword"
tp["do"] = "keyword"
tp["in"] = "keyword"
tp["end"] = "keyword"
tp["if"] = "keyword"
tp["then"] = "keyword"
tp["else"] = "keyword"
tp["elseif"] = "keyword"
tp["for"] = "keyword"
tp["while"] = "keyword"
tp["repeat"] = "keyword"
tp["until"] = "keyword"
tp.setcolor("#C")

local intpattern = "%d+"
function tp:intliteral(c, fragment)
	local newfrag = fragment..c
	if not newfrag:match('^'..intpattern..'$') then
		if #fragment == 0 then
			return nil, true
		else
			return fragment
		end
	end
end

local escapechars = {
	n = "\n",
	r = "\r",
	t = "\t",
	["\\"] = "\\",
	["\""] = "\"",
	["\'"] = "\'",
}

function tp:stringliteral(c, fragment)
	if #fragment == 0 then
		if c:match("[\"\']") then
			self.value = ""
			return
		else
			return nil, true
		end
	else
		if #fragment >= 2 then
			if fragment:sub(1, 1) == fragment:sub(-1, -1) and (#fragment == 2 or fragment:sub(-2, -2) ~= '\\') then
				return self.value:sub(1, -2)
			end
		end
		self.value = self.value..c
	end
end

tp.setcolor("#f")
tp["="] = "assign"
for op, name in pairs({
	["+"] = "add",
	["-"] = "subtract",
	["*"] = "multiply",
	["/"] = "divide",
	["%"] = "modulus",
	[".."] = "concat"
}) do
	tp[op.."="] = name.." assign"
	tp[op] = name
end
tp["=="] = "equal"
tp["!="] = "not equal"
tp["~="] = "not equal"
for op, name in pairs({
	["<"] = "less than",
	[">"] = "greater than",
}) do
	tp[op] = name
	local assignop = op.."="
end


tp.setcolor("#bM")
tp["."] = "dot"
tp[":"] = "colon"
tp[";"] = "semicolon"
tp[","] = "comma"
tp["#"] = "lengthof"

tp.setcolor("#bY")
tp["["] = "lbracket"
tp["]"] = "rbracket"
tp["{"] = "lbrace"
tp["}"] = "rbrace"
tp["("] = "lparen"
tp[")"] = "rparen"

tp["#!"] = "shebang"
